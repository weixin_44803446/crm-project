package cn.umbrella.crm_core;

import cn.umbrella.crm_core.workbench.domain.Activity;


import cn.umbrella.crm_core.workbench.service.impl.ActivityServiceImpl;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

public class ExcelReadTest{
    @Test
    public void testReadActivitiesExcel(){

        String fileName = "";
        fileName = "C:\\Users\\Aerle\\Desktop\\市场活动导入模板.xlsx";
        // 一个文件一个reader
        try (ExcelReader excelReader = EasyExcel.read(fileName, Activity.class, new ActivityReadListener()).build()) {
            // 构建一个sheet 这里可以指定名字或者no
            ReadSheet readSheet = EasyExcel.readSheet(0).build();
            // 读取一个sheet
            excelReader.read(readSheet);
        }


    }
}
