package cn.umbrella.crm_core;

import cn.umbrella.crm_core.commons.utils.DateUtils;
import cn.umbrella.crm_core.workbench.domain.Activity;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WriteExcelTest {

    @Test
    public void testExcelWrite(){
        // 定义对象列表
        List<Activity> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Activity data = new Activity();
            data.setName("字符串" + i);
            data.setCreateTime(DateUtils.dateToString(new Date()));
            list.add(data);
        }
        String fileName = "C:\\Users\\Aerle\\Desktop\\测试测试.xlsx";
        // 这里 需要指定写用哪个class去写
        try (ExcelWriter excelWriter = EasyExcel.write(fileName, Activity.class).build()) {
            WriteSheet writeSheet = EasyExcel.writerSheet("模板").build();
            excelWriter.write(list, writeSheet);
        }
    }
}
