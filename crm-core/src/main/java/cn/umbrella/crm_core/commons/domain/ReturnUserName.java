package cn.umbrella.crm_core.commons.domain;

/**
 * 这个类用于返回session中的用户信息，用于在页面中反馈
 */
public class ReturnUserName {
    String userName;

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

}
