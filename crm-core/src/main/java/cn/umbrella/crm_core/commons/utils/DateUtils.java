package cn.umbrella.crm_core.commons.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtils {

    /**
     * 比较日期是否过期
     *
     */
    public static boolean isBefore(String expireDateString) {
	// 日期转换
	DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	// 用户账户有效期转换
	LocalDate expireDate = LocalDate.parse(expireDateString, dateTimeFormatter);
	LocalDate nowDate = LocalDate.now();
	// 判断账户有效期是否在当前日期前面
	return expireDate.isBefore(nowDate);
    }

    /**
     * 日期格式化工具，用于将日期对象格式化为字符串
     *
     */
    public static String dateToString(Date date) {
	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    /**
     * 返回日期及时间的格式化字符串
     * @param date 日期对象
     * @return 格式化后的字符串
     */
    public static String timeToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }
}
