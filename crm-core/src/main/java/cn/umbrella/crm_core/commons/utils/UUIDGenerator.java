package cn.umbrella.crm_core.commons.utils;

import java.util.UUID;

public class UUIDGenerator {

    public static String generateUUID() {
	return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
