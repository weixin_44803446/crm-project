package cn.umbrella.crm_core.commons.domain;

/**
 * 用来封装返回到前端页面的内容
 */
public class ResponseResult {
    String code;
    String message;
    Object others;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String massage) {
        this.message = massage;
    }

    public Object getOthers() {
        return others;
    }

    public void setOthers(Object others) {
        this.others = others;
    }

    @Override
    public String toString() {
        return "ResponseResult [code=" + code + ", massage=" + message + ", others=" + others + "]";
    }

}
