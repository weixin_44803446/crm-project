package cn.umbrella.crm_core.commons.utils;

import cn.umbrella.crm_core.workbench.domain.Activity;
import com.alibaba.excel.EasyExcel;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * 处理Excel的工具集
 */
public class ExcelTools {
    /**
     * 用于将一个List列表文件写出到网页端
     * @param response HttpServletResponse
     * @param data 一个对象列表，也就是要写出去的数据
     * @param fileName 传入一个文件的名字
     */
    public static void writeActivities2Web(HttpServletResponse response, List<Activity> data, String fileName){
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        try{
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            EasyExcel.write(response.getOutputStream(), Activity.class).sheet("模板").doWrite(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
