package cn.umbrella.crm_core.commons.contants;

/**
 * 用于各种常量
 */
public class CONTANTS_CODE {

    // 保存用于返回代码的常量
    public static final String SUCCESS = "1";
    public static final String FAIL = "0";

    // 用于保存session的key的常量
    public static final String SESSION_USER = "sessionUser";
}
