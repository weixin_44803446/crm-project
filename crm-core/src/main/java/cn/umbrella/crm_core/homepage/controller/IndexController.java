package cn.umbrella.crm_core.homepage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 用户浏览器输入http://127.0.0.1:9899/crm/，跳转到IndexController进行处理
 * IndexController跳转index.jsp页面
 */
@Controller
public class IndexController {
	@RequestMapping("/")
	public ModelAndView index() {
        return new ModelAndView("/index");
	}

}
