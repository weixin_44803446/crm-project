package cn.umbrella.crm_core.settings.controller;

import cn.umbrella.crm_core.commons.contants.CONTANTS_CODE;
import cn.umbrella.crm_core.commons.domain.ResponseResult;
import cn.umbrella.crm_core.commons.utils.DateUtils;
import cn.umbrella.crm_core.settings.domain.User;
import cn.umbrella.crm_core.settings.service.UserService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 用户请求主页登录页面
     *
     */
    @RequestMapping("/settings/qx/user/goLogin.do")
    public ModelAndView toLogin() {
        return new ModelAndView("settings/qx/user/login");
    }

    /**
     * 用户输入账号及密码，请求登录
     * 
     * @param loginAcc     账号
     * @param loginPwd     密码
     * @param isRemembered 是否记住密码
     */
    @RequestMapping("/settings/qx/user/login.do")
    @ResponseBody
    public Object login(HttpServletRequest request, String loginAcc, String loginPwd, String isRemembered,
	    HttpSession session, HttpServletResponse response) {
	ResponseResult responsInfo = new ResponseResult();
	Map<String, Object> requestInfo = new HashMap<>();
	requestInfo.put("loginAcc", loginAcc);
	requestInfo.put("loginPwd", loginPwd);
	// 查询用户信息
	User user = userService.queryUserByAccandPwd(requestInfo);
	if (user == null) {
	    // 登录失败，账号或密码失败
	    responsInfo.setCode(CONTANTS_CODE.FAIL);
	    responsInfo.setMessage("登录失败，账号或密码错误");
	} else if (DateUtils.isBefore(user.getExpireTime())) {
	    // 判断账户有效期是否在当前日期前面
	    // 登录失败，账号已过有效期
	    responsInfo.setCode(CONTANTS_CODE.FAIL);
	    responsInfo.setMessage("登录失败，账号已过有效期");
	} else if (user.getLockState().equalsIgnoreCase(CONTANTS_CODE.FAIL)) {
	    // 判断账号是否被锁定
	    // 登录失败，账号已被锁定 responsInfo.setCode(RETURN_CODE.FAIL);
	    responsInfo.setMessage("登录失败，账号已被锁定");
	} else if (!user.getAllowIps().contains(request.getRemoteAddr())) {
	    // 判断用户的IP地址是否在常用列表内
	    // 登录失败，用户IP禁止登录
	    responsInfo.setCode(CONTANTS_CODE.FAIL);
	    responsInfo.setMessage("登录失败，用户IP禁止登录");
	} else {
	    // 登录成功
	    responsInfo.setCode(CONTANTS_CODE.SUCCESS);
	    session.setAttribute(CONTANTS_CODE.SESSION_USER, user);
	    // 判断是否记住密码，从而决定是否写入cookie
	    if (isRemembered.equals("true")) {
		Cookie cookieUserName = new Cookie("loginAcc", user.getLoginAct());
		cookieUserName.setMaxAge(10 * 24 * 60 * 60);
		cookieUserName.setHttpOnly(true);
		response.addCookie(cookieUserName);
		Cookie cookieUserPwd = new Cookie("loginPwd", user.getLoginPwd());
		cookieUserName.setMaxAge(10 * 24 * 60 * 60);
		cookieUserPwd.setHttpOnly(true);
		response.addCookie(cookieUserPwd);
	    } else {
			System.out.println("登录成功！");
		Cookie cookieUserName = new Cookie("loginAcc", "1");
		cookieUserName.setMaxAge(0);
		response.addCookie(cookieUserName);
		Cookie cookieUserPwd = new Cookie("loginPwd", "1");
		cookieUserName.setMaxAge(0);
		response.addCookie(cookieUserPwd);
	    }
	}
	return responsInfo;
    }

    /**
     * 安全退出功能
     *
     */
    @RequestMapping("/settings/qx/user/logout.do")
    public ModelAndView logOut(HttpServletResponse response, HttpSession session) {
	ModelAndView mv = new ModelAndView("redirect:/");
	// 清空cookie
	Cookie cookieUserName = new Cookie("loginAcc", "1");
	cookieUserName.setMaxAge(0);
	response.addCookie(cookieUserName);
	Cookie cookieUserPwd = new Cookie("loginPwd", "1");
	cookieUserName.setMaxAge(0);
	response.addCookie(cookieUserPwd);
	// 清空session
	session.invalidate();
	return mv;
    }

}
