package cn.umbrella.crm_core.settings.service.impl;

import cn.umbrella.crm_core.settings.domain.User;
import cn.umbrella.crm_core.settings.mapper.UserMapper;
import cn.umbrella.crm_core.settings.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("UserService")
public class UserServiceImpl implements UserService {
    // 注入UserMapper对象
    private final UserMapper userMapper;

    public UserServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    /**
     * 通过用户名及密码查找用户
     */
    @Override
    public User queryUserByAccandPwd(Map<String, Object> loginInfo) {
        User user = null;
        try{
           user = userMapper.selectUserByAccandPwd(loginInfo);
        }catch (Exception e){
            e.printStackTrace();
        }
        return user;

    }

    /**
     * 查找所有用户的基本信息，用于前台展示
     */
    @Override
    public List<User> queryAllUserInfo() {
	return userMapper.selectAllUsers();
    }

}
