package cn.umbrella.crm_core.settings.service;

import cn.umbrella.crm_core.settings.domain.User;

import java.util.List;
import java.util.Map;

/**
 * 用户信息操作业务层
 */
public interface UserService {
    /**
     * 通过用户名及用户密码查找用户信息
     * 
     * @param loginInfo
     * @return
     */
    User queryUserByAccandPwd(Map<String, Object> loginInfo);

    /**
     * 查询所有用户列表
     * 
     * @return
     */
    List<User> queryAllUserInfo();
}
