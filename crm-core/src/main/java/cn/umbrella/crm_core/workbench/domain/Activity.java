package cn.umbrella.crm_core.workbench.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ContentRowHeight(20)
@HeadRowHeight(18)
@ColumnWidth(18)
@EqualsAndHashCode
public class Activity {
    @ColumnWidth(33)
    @ExcelProperty(value = "ID")
    private String id;

    @ExcelProperty(value = "所有者")
    private String owner;

    @ExcelProperty(value = "活动名称")
    private String name;

    @ExcelProperty(value = "开始日期")
    private String startDate;

    @ExcelProperty(value = "结束时间")
    private String endDate;

    @ExcelProperty(value = "成本")
    private String cost;

    @ExcelProperty(value = "详细介绍")
    private String description;

    @ExcelProperty(value = "创建时间")
    private String createTime;

    @ExcelProperty(value = "创建人")
    private String createBy;

    @ExcelProperty(value = "修改时间")
    private String editTime;

    @ExcelProperty(value = "修改人")
    private String editBy;

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public void setOwner(String owner) {
        this.owner = owner == null ? null : owner.trim();
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }


    public void setStartDate(String startDate) {
        this.startDate = startDate == null ? null : startDate.trim();
    }


    public void setEndDate(String endDate) {
        this.endDate = endDate == null ? null : endDate.trim();
    }


    public void setCost(String cost) {
        this.cost = cost == null ? null : cost.trim();
    }


    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }


    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }


    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }


    public void setEditTime(String editTime) {
        this.editTime = editTime == null ? null : editTime.trim();
    }


    public void setEditBy(String editBy) {
        this.editBy = editBy == null ? null : editBy.trim();
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id='" + id + '\'' +
                ", owner='" + owner + '\'' +
                ", name='" + name + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", cost='" + cost + '\'' +
                ", description='" + description + '\'' +
                ", createTime='" + createTime + '\'' +
                ", createBy='" + createBy + '\'' +
                ", editTime='" + editTime + '\'' +
                ", editBy='" + editBy + '\'' +
                '}';
    }
}