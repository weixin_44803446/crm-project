package cn.umbrella.crm_core.workbench.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WorkbenchMainController {

    /**
     * 当登录成功后，请求显示工作台主页
     * 
     * @return
     */
    @RequestMapping("/workbench/main/index.do")
    public ModelAndView mainIndex() {
	ModelAndView mv = new ModelAndView("workbench/main/index");
	return mv;
    }

}
