package cn.umbrella.crm_core.workbench.controller;

import cn.umbrella.crm_core.commons.contants.CONTANTS_CODE;
import cn.umbrella.crm_core.commons.domain.ReturnUserName;
import cn.umbrella.crm_core.settings.domain.User;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WorkbenchIndexController {

    /**
     * 跳转工作台页面
     * 
     * @return
     */
    @RequestMapping("/workbench/index.do")
    public ModelAndView index() {
	ModelAndView mv = new ModelAndView("workbench/index");
	return mv;
    }

    /**
     * 这个方法主要用于返回session中的用户信息用于在页面显示
     * 
     * @param session
     * @return
     */
    @RequestMapping("/workbench/getName.do")
    @ResponseBody
    public ReturnUserName querySessionUserName(HttpSession session) {
	ReturnUserName returnUserName = new ReturnUserName();
	User user = (User) session.getAttribute(CONTANTS_CODE.SESSION_USER);
	if (user != null) {
	    String userName = user.getName();
	    returnUserName.setUserName(userName);
	    // System.out.println(returnUserName);
	} else {
	    System.out.println("没有用户信息");
	}

	return returnUserName;

    }
}
