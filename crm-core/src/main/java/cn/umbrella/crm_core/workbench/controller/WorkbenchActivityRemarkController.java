package cn.umbrella.crm_core.workbench.controller;

import cn.umbrella.crm_core.commons.contants.CONTANTS_CODE;
import cn.umbrella.crm_core.commons.domain.ResponseResult;
import cn.umbrella.crm_core.commons.utils.DateUtils;
import cn.umbrella.crm_core.commons.utils.UUIDGenerator;
import cn.umbrella.crm_core.settings.domain.User;
import cn.umbrella.crm_core.settings.service.UserService;
import cn.umbrella.crm_core.workbench.domain.ActivitiesRemark;
import cn.umbrella.crm_core.workbench.service.ActivityRemarkService;
import cn.umbrella.crm_core.workbench.service.ActivityService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

@Controller
public class WorkbenchActivityRemarkController {

    @Autowired
    private UserService userService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ActivityRemarkService activityRemarkService;
    /**
     * 新增备注
     * @param activitiesRemark 封装的备注对象
     * @return 返回结果
     */
    @RequestMapping("/workbench/activity/addNewRemark.do")
    @ResponseBody
    public Object addNewRemark(HttpSession session, ActivitiesRemark activitiesRemark){
        ResponseResult rs = new ResponseResult();
        try {
            if (activitiesRemark != null) {
                User currentUser = (User) session.getAttribute(CONTANTS_CODE.SESSION_USER);
                activitiesRemark.setCreateBy(currentUser.getId());
                activitiesRemark.setCreateTime(DateUtils.timeToString(new Date()));
                activitiesRemark.setId(UUIDGenerator.generateUUID());
                System.out.println(activitiesRemark);
                int added = activityRemarkService.addActivityRemark(activitiesRemark);
                if (added > 0) {
                    rs.setCode(CONTANTS_CODE.SUCCESS);
                } else {
                    rs.setCode(CONTANTS_CODE.FAIL);
                    rs.setMessage("插入备注失败，请稍后再试！");
                }
            } else {
                rs.setCode(CONTANTS_CODE.FAIL);
                rs.setMessage("网络繁忙，请稍后再试！");
            }
        }catch (Exception e){
            rs.setCode(CONTANTS_CODE.FAIL);
            rs.setMessage(e.getMessage());
        }
        return rs;
    }

    /**
     * 通过备注ID删除备注
     * @param id 备注的ID
     * @return 返回结果
     */
    @RequestMapping("/workbench/activity/removeActivityRemarkById.do")
    @ResponseBody
    public Object removeActivityRemarkById(String id){
        ResponseResult rs = new ResponseResult();
        try{
            if(id != null && !id.isEmpty()){
               int ret = activityRemarkService.deleteActivityRemarkById(id);
               if (ret > 0) {
                   rs.setCode(CONTANTS_CODE.SUCCESS);
               }
            }
        }catch (Exception e){
            rs.setCode(CONTANTS_CODE.FAIL);
            rs.setMessage(e.getMessage());
        }
        return rs;
    }

    @RequestMapping("/workbench/activity/modifyActivityRemarkById.do")
    @ResponseBody
    public Object modifyActivityRemarkById(HttpSession session, ActivitiesRemark activitiesRemark){
        ResponseResult rs = new ResponseResult();
        try{
            if (activitiesRemark != null) {
                User currentUser = (User) session.getAttribute(CONTANTS_CODE.SESSION_USER);
                activitiesRemark.setEditBy(currentUser.getId());
                activitiesRemark.setEditTime(DateUtils.timeToString(new Date()));
                activitiesRemark.setEditFlag("1");
                int ret = activityRemarkService.updateActivityRemark(activitiesRemark);
                if (ret > 0) {
                    rs.setCode(CONTANTS_CODE.SUCCESS);
                }else{
                    rs.setCode(CONTANTS_CODE.FAIL);
                    rs.setMessage("修改失败");
                }
            }
        }catch (Exception e){
            rs.setCode(CONTANTS_CODE.FAIL);
            rs.setMessage(e.getMessage());
        }
        return rs;
    }
}
