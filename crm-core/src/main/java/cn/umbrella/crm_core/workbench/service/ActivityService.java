package cn.umbrella.crm_core.workbench.service;

import cn.umbrella.crm_core.workbench.domain.Activity;

import java.util.List;
import java.util.Map;

public interface ActivityService {
    /**
     * 
     * 创建新的市场活动
     * 
     * @param activity
     * @return
     */
    int createNewActivity(Activity activity);
    
    /**
     * 通过条件查询市场活动列表
     * @param conditions
     * @return
     */
    List<Activity> queryActivityByConditionsForPage(Map<String,Object> conditions);
    
    /**
     * 查询对应条件下的市场活动总条数
     * @param conditions 条件
     * @return 查询到的总条数
     */
    int queryActivityCountsByConditions(Map<String,Object> conditions);

    /**
     *
     * @param ids 传入要删除的市场活动id数组
     * @return 删除的条数
     */
    int deleteActivities(List<String> ids);

    /**
     * 通过市场活动的ID来查询单个的市场活动对象
     * @param id 单个市场活动的对象
     * @return 返回单个市场活动对象
     */
    Activity queryActivityById(String id);

    /**
     * 修改市场活动
     * @param activity 市场活动对象
     * @return 是否修改成功
     */
    int modifyActivity(Activity activity);

    /**
     * 查询所有的市场活动用于导出
     * @return 返回所有市场活动列表
     */
    List<Activity> queryAllActivities();

    /**
     * 通过市场活动的ID列表查询市场活动
     * @param ids 市场活动的ID列表
     * @return 市场活动对象列表
     */
    List<Activity> queryActivitiesByIds(String[] ids);

    /**
     * 测试Excel读取模块，打印收到的list
     */
    void printActivities(List<Activity> activities);

    /**
     * 批量插入市场活动
     * @param activities 市场活动列表
     * @return 插入的总条数
     */
    int addActivitiesByList(List<Activity> activities,String currentUser);

    /**
     * 通过ID查找市场活动信息，用于市场活动详情显示
     * @param id 市场活动的ID
     * @return 查询到的单个的市场活动
     */
    Activity queryActivityForDetails(String id);
}
