package cn.umbrella.crm_core.workbench.service.impl;

import cn.umbrella.crm_core.commons.utils.DateUtils;
import cn.umbrella.crm_core.commons.utils.UUIDGenerator;
import cn.umbrella.crm_core.settings.domain.User;
import cn.umbrella.crm_core.settings.mapper.UserMapper;
import cn.umbrella.crm_core.workbench.domain.Activity;
import cn.umbrella.crm_core.workbench.mapper.ActivityMapper;
import cn.umbrella.crm_core.workbench.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("activityService")
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityMapper activityMapper;
    @Autowired
    private UserMapper userMapper;


    @Override
    public int createNewActivity(Activity activity) {
        return activityMapper.insertNewActivity(activity);
    }

    @Override
    public List<Activity> queryActivityByConditionsForPage(Map<String, Object> conditions) {
	return activityMapper.selectActivityListByConditionForPage(conditions);
    }

    @Override
    public int queryActivityCountsByConditions(Map<String, Object> conditions) {
	return activityMapper.selectActivityCounts(conditions);
    }

    @Override
    public int deleteActivities(List<String> ids) {
        if(ids == null){
            return 0;
        }
        return activityMapper.deleteActivitiesByIdArray(ids);
    }

    @Override
    public Activity queryActivityById(String id) {
        return activityMapper.selectActivityById(id);
    }

    @Override
    public int modifyActivity(Activity activity) {
        return activityMapper.updateActivityById(activity);
    }

    @Override
    public List<Activity> queryAllActivities() {
        return activityMapper.selectAllActivities();
    }

    @Override
    public List<Activity> queryActivitiesByIds(String[] ids) {
        return activityMapper.selectActivitiesByIds(ids);
    }

    @Override
    public void printActivities(List<Activity> activities) {
        for (Activity activity : activities) {
            System.out.println(activity);
        }
    }

    @Override
    public int addActivitiesByList(List<Activity> activities, String currentUser) {
        String insertTime = DateUtils.dateToString(new Date());
        int failedNum = 0;
        for (Activity activity : activities) {
           User owner = userMapper.selectUserIdByName(activity.getOwner());
           if(owner == null){
               failedNum++;
           }else{
               activity.setId(UUIDGenerator.generateUUID());
               activity.setOwner(owner.getId());
               activity.setCreateBy(currentUser);
               activity.setCreateTime(insertTime);
           }
        }
        int all = activityMapper.insertActivitiesByList(activities);
        return failedNum;
    }

    @Override
    public Activity queryActivityForDetails(String id) {
        return activityMapper.selectActivityForDetail(id);
    }
}
