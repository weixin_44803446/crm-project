package cn.umbrella.crm_core.workbench.service.impl;

import cn.umbrella.crm_core.workbench.domain.ActivitiesRemark;
import cn.umbrella.crm_core.workbench.mapper.ActivitiesRemarkMapper;
import cn.umbrella.crm_core.workbench.service.ActivityRemarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("activityRemarkService")
public class ActivityRemarkServiceImpl implements ActivityRemarkService {

    @Autowired
    private ActivitiesRemarkMapper activitiesRemarkMapper;

    @Override
    public List<ActivitiesRemark> getActivitiesRemarkByActivityId(String activityId) {
        return activitiesRemarkMapper.selectActivitiesRemarksByActivityId(activityId);
    }

    @Override
    public int addActivityRemark(ActivitiesRemark activitiesRemark) {
        return activitiesRemarkMapper.insertActivityRemark(activitiesRemark);
    }

    @Override
    public int deleteActivityRemarkById(String remarkId) {
        return activitiesRemarkMapper.deleteActivityRemarkById(remarkId);
    }

    @Override
    public int updateActivityRemark(ActivitiesRemark activitiesRemark) {
        return activitiesRemarkMapper.updateActivityRemarkById(activitiesRemark);
    }
}
