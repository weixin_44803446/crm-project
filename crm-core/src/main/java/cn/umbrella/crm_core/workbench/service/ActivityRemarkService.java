package cn.umbrella.crm_core.workbench.service;

import cn.umbrella.crm_core.workbench.domain.ActivitiesRemark;

import java.util.List;

public interface ActivityRemarkService {
    /**
     * 通过市场活动ID获取备注
     * @param activityId 市场活动ID
     * @return 备注列表
     */
    List<ActivitiesRemark> getActivitiesRemarkByActivityId(String activityId);

    /**
     * 插入新的备注
     * @param activitiesRemark 新的备注内容
     * @return 影响的行数
     */
    int addActivityRemark(ActivitiesRemark activitiesRemark);

    /**
     * 通过备注ID删除备注
     * @param remarkId 备注的ID
     * @return 影响的行数
     */
    int deleteActivityRemarkById(String remarkId);

    /**
     * 更新备注内容
     * @param activitiesRemark 更新后的备注对象
     * @return 返回影响的行数
     */
    int updateActivityRemark(ActivitiesRemark activitiesRemark);
}
