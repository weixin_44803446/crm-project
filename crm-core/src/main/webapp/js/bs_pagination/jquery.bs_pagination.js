/**
 * @fileOverview bs_pagination: jQuery pagination plugin, based on Twitter Bootstrap.
 *               <p>License MIT
 *               <br />Copyright Christos Pontikis <a href="http://pontikis.net">http://pontikis.net</a>
 *               <br />Project page <a href="http://www.pontikis.net/labs/bs_pagination">http://www.pontikis.net/labs/bs_pagination</a>
 * @version 1.0.5 (19 June 2024)
 * @author Christos Pontikis http://www.pontikis.net
 * @requires jquery >= 3.7.1, twitter bootstrap >= 4
 * 为了学习编程发布
 */

/**
 * See <a href="http://jquery.com">http://jquery.com</a>.
 * @name $
 * @class
 * See the jQuery Library  (<a href="http://jquery.com">http://jquery.com</a>) for full details.  This just
 * documents the function and classes that are added to jQuery by this plug-in.
 */

/**
 * See <a href="http://jquery.com">http://jquery.com</a>
 * @name fn
 * @class
 * See the jQuery Library  (<a href="http://jquery.com">http://jquery.com</a>) for full details.  This just
 * documents the function and classes that are added to jQuery by this plug-in.
 * @memberOf $
 */

/**
 * Pseudo-Namespace containing private methods (for documentation purposes)
 * @name _private_methods
 * @namespace
 */

"use strict";
(function($) {

    /**
         * 禁用选择（适用于jquery 3.7）
         * @param element - 要禁用选择的 jQuery 对象或选择器字符串
         * @return {jQuery} - 返回原始的 jQuery 对象以便链式调用
         */
    const disableSelection = function(element) {
        // 确保 element 是一个 jQuery 对象
        element = $(element);

        // 使用 prop 而不是 attr 来设置 unselectable 属性（虽然这不是所有浏览器都支持）
        element.prop("unselectable", "on");

        // 使用 css 设置 user-select 属性
        element.css("user-select", "none");
        // 对于某些旧版浏览器，可能还需要设置以下属性
        element.css("-moz-user-select", "none"); // Firefox
        element.css("-ms-user-select", "none"); // IE/Edge
        element.css("-webkit-user-select", "none"); // Safari, Chrome

        // 使用 on 方法绑定一个处理程序来阻止 selectstart 事件
        element.on("selectstart", function(e) {
            e.preventDefault();
            return false; // 虽然在这里 return false 是多余的，但为了明确性可以保留
        });

        // 返回原始的 jQuery 对象以便链式调用
        return element;
    };
    /**
         * 核心！！改变页码的方法
         * @param container_id
         * @param previous_selection
         * @param current_selection
         * @param update_nav_items
         * @param trigger_change_page
         */
    const change_page = function(container_id, previous_selection, current_selection, update_nav_items, trigger_change_page) {
        // 重置选项，定义变量
        const elem = $("#" + container_id),
            s = methods.getAllOptions.call(elem),
            nav_item_id_prefix = create_id(s.nav_item_id_prefix, container_id) + "_";
        // 如果按下按钮后，分页组件需要变化
        if(update_nav_items) {
            let nav_list = create_id(s.nav_list_id_prefix, container_id),
                nav_top_id = create_id(s.nav_top_id_prefix, container_id),
                nav_prev_id = create_id(s.nav_prev_id_prefix, container_id),
                nav_next_id = create_id(s.nav_next_id_prefix, container_id),
                nav_last_id = create_id(s.nav_last_id_prefix, container_id),
                elem_nav_list = $("#" + nav_list),
                nav_html = "",
                nav_start = parseInt(s.currentPage),
                nav_end,
                i, mod, offset, totalSections,
                nav_url = "",
                no_url = "javascript:void(0);";
            // 分页导航的页码
            if(s.totalPages < s.visiblePageLinks) {
                nav_start = 1;
                nav_end = s.totalPages;
            } else {
                totalSections = Math.ceil(s.totalPages / s.visiblePageLinks);
                if(nav_start > s.visiblePageLinks * (totalSections - 1)) {
                    nav_start = s.totalPages - s.visiblePageLinks + 1;
                } else {
                    mod = nav_start % s.visiblePageLinks;
                    offset = mod === 0 ? -s.visiblePageLinks + 1 : -mod + 1;
                    nav_start += offset;
                }
                nav_end = nav_start + s.visiblePageLinks - 1;
            }
            // 保存起始页码及终止页码
            elem.data("nav_start", nav_start);
            elem.data("nav_end", nav_end);
            // 渲染分页组件
            // 根据条件判断是否显示首页及上一页导航控件
            if(nav_start > 1) {
                nav_url = s.directURL ? s.directURL(1) : no_url;
                nav_html += '<li class="page-item"><a class="page-link" id="' + nav_top_id + '" href="' + nav_url + '">' + rsc_bs_pag.go_top_text + '</a></li>';
                nav_url = s.directURL ? s.directURL(nav_start - 1) : no_url;
                nav_html += '<li class="page-item"><a class="page-link" id="' + nav_prev_id + '" href="' + nav_url + '">' + rsc_bs_pag.go_prev_text + '</a></li>';
            }
            // 显示页码选项
            for(i = nav_start; i <= nav_end; i++) {
                nav_url = s.directURL ? s.directURL(i) : no_url;
                nav_html += '<li class="page-item"><a class="page-link" id="' + nav_item_id_prefix + i + '" href="' + nav_url + '">' + i + '</a></li>';
            }
            // 根据条件判断是否显示尾页及下一页控件
            if(nav_end < s.totalPages) {
                nav_url = s.directURL ? s.directURL(nav_end + 1) : no_url;
                nav_html += '<li class="page-item"><a class="page-link" id="' + nav_next_id + '" href="' + nav_url + '">' + rsc_bs_pag.go_next_text + '</a></li>';
                nav_url = s.directURL ? s.directURL(s.totalPages) : no_url;
                nav_html += '<li class="page-item"><a class="page-link" id="' + nav_last_id + '" href="' + nav_url + '">' + rsc_bs_pag.go_last_text + '</a></li>';
            }
            elem_nav_list.html(nav_html);
            if(s.disableTextSelectionInNavPane) {
                disableSelection(elem_nav_list);
            }
        }
        // 重置选项
        var prev_elem = $("#" + nav_item_id_prefix + previous_selection),
            current_elem = $("#" + nav_item_id_prefix + current_selection);
        // 更改所选页面，应用适当的样式
        prev_elem.closest("li").removeClass(s.navListActiveItemClass);
        current_elem.closest("li").addClass(s.navListActiveItemClass);
        // 更新title
        var active_title = rsc_bs_pag.current_page_label + " " + current_selection + " " + rsc_bs_pag.total_pages_label + " " + s.totalPages;
        prev_elem.prop("title", "");
        current_elem.prop("title", active_title);
        if(s.showRowsInfo && s.showRowsDefaultInfo) {
            var page_first_row = ((s.currentPage - 1) * s.rowsPerPage) + 1,
                page_last_row = Math.min(page_first_row + s.rowsPerPage - 1, s.totalRows),
                info_html = page_first_row + "-" + page_last_row + " " +
                    rsc_bs_pag.total_rows_label + " " + s.totalRows + " " + rsc_bs_pag.rows_info_records +
                    " (" + rsc_bs_pag.current_page_abbr_label + s.currentPage + rsc_bs_pag.total_pages_abbr_label + s.totalPages + ")",
                rows_info_id = create_id(s.nav_rows_info_id_prefix, container_id);
            $("#" + rows_info_id).html(info_html);
        }
        // ChangePage上的触发器事件（仅在按下某些链接后，而不是在加载插件时）
        if(trigger_change_page) {
            elem.triggerHandler("onChangePage", {currentPage: current_selection, rowsPerPage: s.rowsPerPage});
        } else {
            elem.triggerHandler("onLoad", {currentPage: current_selection, rowsPerPage: s.rowsPerPage});
        }
    };
    const pluginName = "bs_pagination";

    /* 公共方法 */
    const methods = {

        /**
         * @lends $.fn.bs_pagination
         */
        init: function (options) {

            const elem = this;

            return this.each(function () {

                /**
                 * 设置及默认值
                 * 使用$.extend，设置修改将影响elem.data（），反之亦然
                 */
                let settings = elem.data(pluginName);
                if (typeof settings == "undefined") {
                    // 这里把Bootstrap的版本改为4
                    let bootstrap_version = "5";
                    if (options.hasOwnProperty("bootstrap_version") && options["bootstrap_version"] === "5") {
                        bootstrap_version = "5";
                    }
                    const defaults = methods.getDefaults.call(elem, bootstrap_version);
                    settings = $.extend({}, defaults, options);
                } else {
                    settings = $.extend({}, settings, options);
                }
                if (settings.totalRows === 0) {
                    settings.totalRows = settings.totalPages * settings.rowsPerPage;
                }
                elem.data(pluginName, settings);

                // 绑定变更页码事件
                elem.unbind("onChangePage").bind("onChangePage", settings.onChangePage);
                // 绑定加载事件
                elem.unbind("onLoad").bind("onLoad", settings.onLoad);

                // 重置选项
                var container_id = elem.attr("id"),

                    nav_list_id = create_id(settings.nav_list_id_prefix, container_id),
                    nav_top_id = create_id(settings.nav_top_id_prefix, container_id),
                    nav_prev_id = create_id(settings.nav_prev_id_prefix, container_id),
                    nav_item_id_prefix = create_id(settings.nav_item_id_prefix, container_id) + "_",
                    nav_next_id = create_id(settings.nav_next_id_prefix, container_id),
                    nav_last_id = create_id(settings.nav_last_id_prefix, container_id),
                    goto_page_id = create_id(settings.nav_goto_page_id_prefix, container_id),
                    rows_per_page_id = create_id(settings.nav_rows_per_page_id_prefix, container_id),
                    rows_info_id = create_id(settings.nav_rows_info_id_prefix, container_id),
                    html = "",
                    previous_selection, current_selection,
                    selector_nav_top, selector_nav_prev, selector_nav_pages, selector_nav_next, selector_nav_last,
                    selector_go_to_page, selector_rows_per_page;

                /* 创建导航面板，首先判断Bootstrap版本是不是V5 */
                if (settings.bootstrap_version === "5") {
                    html += '<div class="' + settings.mainWrapperClass + '">';
                    //html += '<nav aria-label="Page navigation">';
                    html += '<div class="' + settings.navListContainerClass + '">';
                    html += '<div class="' + settings.navListWrapperClass + '">';
                    html += '<ul id="' + nav_list_id + '" class="' + settings.navListClass + '">';
                    html += '</ul>';
                    html += '</div>';
                    html += '</div>';
                    // 如果配置了显示跳转页面参数
                    if (settings.showGoToPage && settings.visiblePageLinks < settings.totalPages) {
                        html += '<div class="' + settings.navGoToPageContainerClass + '">';
                        html += '<div class="input-group input-group-sm">';
                        html += '<span class="input-group-text text-sm-center">跳转</span>';
                        html += '<input id="' + goto_page_id + '" type="text" class="' + settings.navGoToPageClass + '" title="' + rsc_bs_pag.go_to_page_title + '">';
                        html += '<span class="input-group-text text-sm-center">页</span>';
                        html += '</div>';
                        html += '</div>';
                    }
                    // 如果配置了显示每页显示条数的参数
                    if (settings.showRowsPerPage) {
                        html += '<div class="' + settings.navRowsPerPageContainerClass + '">';
                        html += '<div class="input-group input-group-sm">';
                        html += '<span class="input-group-text text-sm-center">每页</span>';
                        html += '<input id="' + rows_per_page_id + '" value="' + settings.rowsPerPage + '" type="text" class="' + settings.navRowsPerPageClass + '" title="' + rsc_bs_pag.rows_per_page_title + '">';
                        html += '<span class="input-group-text text-sm-center">条</span>';
                        html += '</div>';
                        html += '</div>';
                    }
                    // 如果配置了显示项目条数信息
                    // if (settings.showRowsInfo) {
                    //     html += '<div class="' + settings.navInfoContainerClass + '">';
                    //     html += '<div id="' + rows_info_id + '" class="' + settings.navInfoClass + '"></div>';
                    //     html += '</div>';
                    // }

                }
                html += '</div>';
                // 将html写入到文档中
                elem.html(html);
                // 前一个选中页数
                previous_selection = null;
                // 当前选中页数
                current_selection = settings.currentPage;
                // 调用改变页数，实际上就是初始化
                change_page(container_id, previous_selection, current_selection, true, false);
                // 使样式生效
                elem.removeClass().addClass(settings.containerClass);

                // 组件的事件们
                if (!settings.directURL) {
                    // 按下跳转首页按钮
                    selector_nav_top = "#" + nav_top_id;
                    elem.off("click", selector_nav_top).on("click", selector_nav_top, function () {
                        const previous_selection = settings.currentPage;
                        settings.currentPage = 1;
                        const current_selection = settings.currentPage;
                        change_page(container_id, previous_selection, current_selection, true, true);
                    });
                    // 按下上一页按钮
                    selector_nav_prev = "#" + nav_prev_id;
                    elem.off("click", selector_nav_prev).on("click", selector_nav_prev, function () {
                        // 判断只有当前页数大于1的时候才可以按上一页
                        if (settings.currentPage > 1) {
                            const previous_selection = settings.currentPage;
                            settings.currentPage = parseInt(settings.currentPage) - 1;
                            const current_selection = settings.currentPage;
                            const recreate_nav = (elem.data("nav_start") === previous_selection);
                            change_page(container_id, previous_selection, current_selection, recreate_nav, true);
                        }
                    });
                    // 按下下一页按钮
                    selector_nav_next = "#" + nav_next_id;
                    elem.off("click", selector_nav_next).on("click", selector_nav_next, function () {
                        if (settings.currentPage < settings.totalPages) {
                            const previous_selection = settings.currentPage;
                            settings.currentPage = parseInt(settings.currentPage) + 1;
                            const current_selection = settings.currentPage;
                            const recreate_nav = (elem.data("nav_end") === previous_selection);
                            change_page(container_id, previous_selection, current_selection, recreate_nav, true);
                        }
                    });
                    // 按下尾页按钮
                    selector_nav_last = "#" + nav_last_id;
                    elem.off("click", selector_nav_last).on("click", selector_nav_last, function () {
                        const previous_selection = settings.currentPage;
                        settings.currentPage = settings.totalPages;
                        const current_selection = settings.currentPage;
                        change_page(container_id, previous_selection, current_selection, true, true);
                    });

                    // 按下页码选项按钮
                    selector_nav_pages = '[id^="' + nav_item_id_prefix + '"]';
                    elem.off("click", selector_nav_pages).on("click", selector_nav_pages, function (event) {
                        const previous_selection = settings.currentPage;
                        const len = nav_item_id_prefix.length;
                        settings.currentPage = parseInt($(event.target).attr("id").substr(len));
                        const current_selection = settings.currentPage;
                        change_page(container_id, previous_selection, current_selection, false, true);
                    });
                }
                // 跳转指定页码
                if (settings.showGoToPage) {
                    selector_go_to_page = "#" + goto_page_id;
                    elem.off("keypress", selector_go_to_page).on("keypress", selector_go_to_page, function (event) {
                        // 按下回车键
                        if (event.which === 13) {
                            let gtp = parseInt($(selector_go_to_page).val());
                            $(selector_go_to_page).val("");
                            if (!isNaN(gtp) && gtp > 0) {
                                if (gtp > settings.totalPages) {
                                    gtp = settings.totalPages;
                                }
                                const previous_selection = settings.currentPage;
                                settings.currentPage = gtp;
                                const current_selection = gtp;
                                if (settings.directURL) {
                                    location.href = settings.directURL(current_selection);
                                } else {
                                    change_page(container_id, previous_selection, current_selection, true, true);
                                }
                            }
                            event.preventDefault(); //如果用户瞎按，就按默认的来，跳转到第一页
                        } else {
                            if (!(event.which === 8 || event.which === 0 || (event.shiftKey === false && (event.which > 47 && event.which < 58)))) {
                                event.preventDefault();
                            }
                        }
                    });
                }
                // 选择每页显示条数设置
                if (settings.showRowsPerPage) {
                    selector_rows_per_page = "#" + rows_per_page_id;
                    elem.off("keypress", selector_rows_per_page).on("keypress", selector_rows_per_page, function (event) {
                        if (event.which === 13) {
                            let rpp = parseInt($(selector_rows_per_page).val());
                            if (!isNaN(rpp) && rpp > 0) {
                                if (rpp > settings.maxRowsPerPage) {
                                    rpp = settings.maxRowsPerPage;
                                }
                                $(selector_rows_per_page).val(rpp);

                                settings.rowsPerPage = rpp;
                                settings.totalPages = Math.ceil(settings.totalRows / settings.rowsPerPage);

                                const previous_selection = settings.currentPage;
                                settings.currentPage = 1;
                                const current_selection = 1;

                                if (settings.directURL) {
                                    location.href = settings.directURL(current_selection);
                                } else {
                                    change_page(container_id, previous_selection, current_selection, true, true);
                                }

                            } else {
                                $(selector_rows_per_page).val(settings.rowsPerPage)
                                alert("每页至少显示1条数据哦！")
                                // selector_rows_per_page.val(settings.rowsPerPage);
                            }
                            event.preventDefault(); // 如果用户瞎按，按默认的来
                        } else {
                            if (!(event.which === 8 || event.which === 0 || (event.shiftKey === false && (event.which > 47 && event.which < 58)))) {
                                event.preventDefault();
                            }
                        }
                    });
                }

            });
        },
        /**
         * 获取当前插件的版本号
         * Get plugin version
         * @returns {string}
         */
        getVersion: function () {
            return "1.0.5";
        },

        /**
         * 定义默认值
         * @example $(element).bs_pagination("getDefaults", "3");
         * @param bootstrap_version
         * @returns {Object}
         */
        getDefaults: function (bootstrap_version) {
            return {
                currentPage: 1,
                rowsPerPage: 10,
                maxRowsPerPage: 100,
                totalPages: 100,
                totalRows: 0,
                visiblePageLinks: 8,
                showGoToPage: true,
                showRowsPerPage: true,
                showRowsInfo: false,
                showRowsDefaultInfo: false,
                directURL: false, // 这个参数的值可以是一个以当前页为参数的函数
                disableTextSelectionInNavPane: true, // 禁用文本选择及双击功能
                bootstrap_version: "5",
                // 主要分页组件的样式
                containerClass: "container-fluid",
                mainWrapperClass: "row justify-content-between no-gutters align-items-center",
                navListContainerClass: "col-8",
                navListWrapperClass: "pagination pagination-sm",
                navListClass: "pagination pagination-sm",
                navListActiveItemClass: "active",
                // 跳转页码的样式
                navGoToPageContainerClass: "col-2",
                navGoToPageClass: "form-control form-control-sm text-sm-center",
                // 设置每页显示条数的样式
                navRowsPerPageContainerClass: "col-2",
                navRowsPerPageClass: "form-control form-control-sm text-sm-center",
                // 显示页码信息的样式
                navInfoContainerClass: "col-xs-12 col-sm-4 col-md-12",
                navInfoClass: "",
                // 定义各个组件的ID前缀
                nav_list_id_prefix: "nav_list_",
                nav_top_id_prefix: "top_",
                nav_prev_id_prefix: "prev_",
                nav_item_id_prefix: "nav_item_",
                nav_next_id_prefix: "next_",
                nav_last_id_prefix: "last_",
                nav_goto_page_id_prefix: "goto_page_",
                nav_rows_per_page_id_prefix: "rows_per_page_",
                nav_rows_info_id_prefix: "rows_info_",
                onChangePage: function () {
                    // 返回页码及每页显示条数
                },
                onLoad: function () {
                    // 当初次加载时，返回页码及每页显示条数
                }
            };
        },

        /**
         * 使用插件名称（字符串形式）获取插件的任何选项集
         * @example $(element).bs_pagination("getOption", some_option);
         * @param opt
         * @return {*}
         */
        getOption: function (opt) {
            const elem = this;
            return elem.data(pluginName)[opt];
        },

        /**
         * 获取所有选项
         * @example $(element).bs_pagination("getAllOptions");
         * @return {*}
         */
        getAllOptions: function () {
            const elem = this;
            return elem.data(pluginName);
        },

        /**
         * 移除插件
         * @example $(element).bs_pagination("destroy");
         */
        destroy: function () {
            const elem = this;
            elem.removeData();
        },

        /**
         * 设置显示条数的信息
         * @example $(element).bs_pagination("setRowsInfo", info_html);
         * @param info_html
         */
        setRowsInfo: function (info_html) {
            const elem = this,
                rows_info_id = create_id(methods.getOption.call(elem, "getOption", "nav_rows_info_id_prefix"), elem.attr("id"));
            $("#" + rows_info_id).html(info_html);
        }
    };

    /* 私有方法 */

    /**
     * @lends _private_methods
     */

    /**
     * 创建元素ID的方法
     * @param prefix
     * @param plugin_container_id
     * @return {*}
     */
    var create_id = function(prefix, plugin_container_id) {
        return prefix + plugin_container_id;
    };


        /**
     * 禁用选择（jquery 1.8） 这个jquery版本有点老，重新写一下这个方法
     * http://stackoverflow.com/questions/2700000/how-to-disable-text-selection-using-jquery
     * @param element
     * @return {*}
    var disableSelection = function(element) {
        return element
            .attr("unselectable", "on")
            .css("user-select", "none")
            .on("selectstart", false);
    };
     */
    /**
     * bs_pagination - jQuery pagination plugin, based on Twitter Bootstrap.
     * bs_pagination-jQuery分页插件，基于Twitter Bootstrap。
     * @class bs_pagination
     * @memberOf $.fn
    */
    $.fn.bs_pagination = function(method) {
        if(this.length !== 1) {
            const err_msg = "You must use this plugin (" + pluginName + ") with a unique element (at once)";
            this.html('<span style="color: red;">' + 'ERROR: ' + err_msg + '</span>');
            $.error(err_msg);
        }
        // 方法调用逻辑
        if(methods[method]) {
            return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if(typeof method === "object" || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error("Method " + method + " does not exist on jQuery." + pluginName);
        }
    };
})(jQuery);